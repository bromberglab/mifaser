name = "mifaser"
__author__ = 'mmiller'
__updated_by__ = 'armalo2@emory.edu, prabakaran@emory.edu, sgramac@emory.edu'
__version__ = '1.63'
__releasedate__ = '09/09/24'
__all__ = [
    'alignment',
    'annotation',
    'caller',
    'cli',
    'external',
    'run',
    'translate'
]

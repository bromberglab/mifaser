FROM python:3.8-alpine as base

LABEL maintainer="mmiller@bromberglab.org" \
      description="mi-faser docker image (https://services.bromberglab.org/mifaser)"

FROM base as builder

# setup system
RUN mkdir /install
WORKDIR /install
# RUN apk update && apk add git && rm -rf /var/cache/apk/*

# setup app
COPY . /app
# RUN pip install --upgrade pip && pip install --prefix=/install -r /app/requirements.txt
# RUN (chmod a+x /app/setup.sh; /app/setup.sh)

FROM base

COPY --from=builder /install /usr/local
COPY --from=builder /app /app

# setup bio-node
LABEL bio-node=v1.0 \
      input_1="fasta_file|fasta_gz|fastq_file|fastq_gz|url|sra_id,-f,required,filename" \
      input_2="fasta_file|fasta_gz|fastq_file|fastq_gz|url|sra_id,-f,optional,filename" \
      input_3="mifaser_config,,static,unquote content" \
      output="mifaser_output,-o,"

# set environment variables
WORKDIR /app
ENV INPUT_PATH=/input OUTPUT_PATH=/output 

# set app ENTRYPOINT
ENTRYPOINT ["python", "-m", "mifaser.cli"]

# set app CMD
CMD ["--version"]
